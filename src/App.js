import "./App.css";
import Header from "./BaitapGlasses/Header";
import Body from "./BaitapGlasses/Body";

function App() {
  return (
    <div className="App">
      <Header />
      <Body />
    </div>
  );
}

export default App;
