import React, { Component } from "react";
const data_glass = [
  {
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 2,
    price: 50,
    name: "GUCCI G8759H",
    url: "./glassesImage/v2.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 3,
    price: 30,
    name: "DIOR D6700HQ",
    url: "./glassesImage/v3.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 4,
    price: 70,
    name: "DIOR D6005U",
    url: "./glassesImage/v4.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 5,
    price: 40,
    name: "PRADA P8750",
    url: "./glassesImage/v5.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 6,
    price: 60,
    name: "PRADA P9700",
    url: "./glassesImage/v6.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 7,
    price: 80,
    name: "FENDI F8750",
    url: "./glassesImage/v7.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 8,
    price: 100,
    name: "FENDI F8500",
    url: "./glassesImage/v8.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
  {
    id: 9,
    price: 60,
    name: "FENDI F4300",
    url: "./glassesImage/v9.png",
    desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
];
export default class Body extends Component {
  state = {
    glass_infor: [],
    glass_list: data_glass,
  };
  handleClick = (item) => {
    this.setState({ glass_infor: item });
  };
  render() {
    const { name, desc, url } = this.state.glass_infor;
    const glass_list = this.state.glass_list;
    return (
      <>
        <div className="container d-flex justify-content-md-around py-4">
          <div className="model" id="glass-information">
            <div id="wear_me">
              <img src={url} width="175px" alt={name}></img>
            </div>
            <div className="pl-1 pt-1" id="glass-content">
              <h5>{name}</h5>
              <p>{desc}</p>
            </div>
          </div>
        </div>
        <div className="container bg-white">
          <div className="row p-3 bg-dark" id="glass_list">
            {glass_list.map((item) => {
              return (
                <button
                  key={item.name}
                  className="button col-2 border-5 bg-white p-2 m-1"
                  onClick={() => {
                    this.handleClick(item);
                  }}
                >
                  <img
                    className="p-1"
                    src={item.url}
                    width="60%"
                    alt={item.name}
                  ></img>
                </button>
              );
            })}
          </div>
        </div>
      </>
    );
  }
}
