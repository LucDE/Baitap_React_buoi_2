import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <header className="py-4">
        <div className="container">
          <h5 className="m-0 text-center text-white font-weight-bold">
            TRY GLASS APP ONLINE
          </h5>
        </div>
      </header>
    );
  }
}
